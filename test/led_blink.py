#!/usr/bin/python

import RPi.GPIO as GPIO
from time import sleep

# Needs to be BCM. GPIO.BOARD lets you address GPIO ports by periperal
# connector pin number, and the LED GPIO isn't on the connector
GPIO.setmode(GPIO.BCM)

# set up GPIO output channel
GPIO.setup(16, GPIO.OUT)

GPIO.output(16, GPIO.LOW)
sleep(1)
GPIO.output(16, GPIO.HIGH)
sleep(1)
GPIO.output(16, GPIO.LOW)
sleep(1)
GPIO.output(16, GPIO.HIGH)
sleep(1)
GPIO.output(16, GPIO.LOW)
sleep(1)
GPIO.output(16, GPIO.HIGH)
sleep(1)

# Problem is that all output are reset to input when done and
# this means that I cannot use the SHell script to control the LED
# Need to find a way to reset the pin 16 to output